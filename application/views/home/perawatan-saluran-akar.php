<section id="news-detail" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-7">
                <!-- NEWS THUMB -->
                <div class="news-detail-thumb">
                    <div class="news-image">
                        <img src="<?= base_url('assets/banner/') . $artikel->images; ?>" class="img-responsive" alt="">
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <!-- SECTION TITLE -->
                            <div class="section-title wow fadeInUp animated" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                <h2 class="text-capitalize">Perawatan Saluran Akar</h2>
                                <p class="text-justify">Salah satu penanganan masalah gigi berlubang adalah dengan cara melakukan perawatan saluran akar (PSA) atau endodontik. Dengan melakukan endodontik gigi kamu yang semula berlubang tidak perlu dicabut dan fungsinya dapat kembali normal. </p>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <!-- SECTION TITLE -->
                            <div class="section-title wow fadeInUp animated" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                <h2 class="text-capitalize">Perawatan Saluran Akar (PSA) atau Endodontik; Tujuan, Indikasi, Prosedur</h2>
                                <p class="text-justify">Perawatan saluran akar (PSA) gigi atau endodontik adalah prosedur untuk mengangkat pulpa gigi, yaitu bagian akar gigi yang terdiri dari pembuluh darah dan saraf gigi. Tindakan ini bertujuan untuk mengobati gigi yang terinfeksi tanpa perlu mencabutnya. </p>
                                <p class="text-justify">Saluran akar gigi (pulpa) merupakan rongga di bagian tengah gigi yang berisi saraf dan pembuluh darah. Jika kondisi ini tidak diobati, bakteri dan bagian pada pulpa yang membusuk dapat menyebabkan infeksi serius atau abses gigi. </p>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <!-- SECTION TITLE -->
                            <div class="section-title wow fadeInUp animated" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                <h2 class="text-capitalize">Tujuan dan Indikasi Perawatan Saluran Akar Gigi</h2>
                                <p class="text-justify">Perawatan saluran akar gigi bertujuan untuk mengangkat pulpa yang terinfeksi bakteri, mencegah terjadinya infeksi berulang, dan menghindari perlunya cabut gigi. Selain itu, perawatan saluran akar gigi juga dapat dilakukan pada orang dengan kondisi berikut: </p>
                                <ul>
                                    <li>
                                        Gigi berlubang (karies gigi) yang dalam dan tidak tertangani dengan baik </li>
                                    <li>
                                        Gigi patah atau mahkota gigi (enamel) rusak
                                    </li>
                                    <li>
                                        Gigi patah atau mahkota gigi (enamel) rusak
                                    </li>
                                    <li>Cedera di gigi</li>
                                    <li>Infeksi di pulpa (pulpitis)</li>
                                </ul>
                                <p class="text-justify">Tanda dan gejala yang mungkin memerlukan perawatan saluran akar gigi adalah </p>
                                <ul>
                                    <li>Gigi nyeri dan ngilu yang menyebar ke rahang, wajah, atau gigi lain</li>
                                    <li>Gusi dan rahang bengkak</li>
                                    <li>Benjolan di gusi</li>
                                    <li>Gigi berwarna hitam</li>
                                    <li>Sakit gigi saat mengunyah makanan</li>
                                    <li>Terdapat daging tumbuh pada lubang gigi (polip pulpa)</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <!-- SECTION TITLE -->
                            <div class="section-title wow fadeInUp animated" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                <h2 class="text-capitalize">Prosedur PSA</h2>
                                <p class="text-left">Prosedur perawatan saluran akar gigi umumnya tidak akan terasa sakit. Ini karena dokter akan memberikan bius lokal tepat sebelum perawatan</p>
                                <p class="text-left">Setelah bius diberikan, berikut alur endodontik yang biasa dilakukan.</p>
                                <ol>
                                    <li>
                                        Mahkota gigi akan dilubangi agar bagian pulpa bisa lebih mudah diakses.
                                    </li>
                                    <li>Dokter memasukkan alat khusus ke dalam pulpa, lalu mematikan dan menghilangkan saraf, pembuluh darah, serta jaringan di dalamnya.</li>
                                    <li>Setelah bagian dalam gigi dihilangkan, rongga pulpa akan dibersihkan, disterilkan, dan dibentuk agar bahan karet pengisi yang disebut gutta-percha bisa lebih mudah masuk ke dalamnya</li>
                                    <li>Dokter mengisi rongga yang kosong dengan gutta-percha.</li>
                                    <li>Lubang pada gigi ditutup dengan tambalan untuk mencegah bakteri mulut masuk kembali</li>
                                </ol>
                                <p>
                                    Biasanya, prosedur ini akan berjalan selama 30 hingga 60 menit. Namun, waktu tersebut bisa lebih lama, tergantung keparahan infeksi dan kerusakan pada gigi.
                                </p>
                                <h3 class="text-capitalize">Setelah Melakukan PSA</h3>
                                <p>Setelah menjalani endodontik, beberapa hal perlu diperhatikan supaya penyembuhan dapat berjalan lancar. Apalagi, ada kemungkinan perawatan yang Anda jalani tidak berhasil</p>
                                <p>Dilansir dari National Health Service (NHS), sejumlah hal yang sebaiknya dilakukan setelah perawatan saluran akar gigi, meliputi</p>
                                <ul>
                                    <li>Minum obat pereda rasa nyeri sesuai resep dokter</li>
                                    <li>Tidak makan makanan penyebab sakit gigi yang bertekstur keras, manis, atau lengket</li>
                                    <li>Tidak merokok</li>
                                    <li>Tidak minum minuman beralkohol</li>
                                    <li>Menyikat gigi dengan benar</li>
                                    <li>Memastikan kebersihan mulut dan gigi terjaga dengan baik.

                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <!-- SECTION TITLE -->
                            <div class="section-title wow fadeInUp animated" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                <h2 class="text-capitalize">Dokter Kami</h2>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <!-- SECTION TITLE -->
                            <div class="section-title wow fadeInUp animated" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                <a href="<?= base_url() ?>">
                                    <img src="<?= base_url('assets/images/fotodokter.jpg') ?>" class="img-responsive" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <!-- SECTION TITLE -->
                            <div class="section-title wow fadeInUp animated" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                <h2 class="text-capitalize">Dokter berpengalaman dan up-to-date</h2>
                                <p class="text-justify">Kami mengadakan pelatihan secara berkala kepada dokter gigi kami agar selalu up-to-date dan tanggap dalam memberikan pelayanan yang memuaskan.</p>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12">
                            <!-- SECTION TITLE -->
                            <div class="section-title wow fadeInUp animated" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                <h2 class="text-capitalize">Peralatan Lengkap</h2>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <!-- NEWS THUMB -->
                            <div class="news-thumb wow fadeInUp animated" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                                <a href="<?= base_url() ?>">
                                    <img src="<?= base_url('assets/images/room1.jpg') ?>" class="img-responsive" alt="">
                                </a>
                                <div class="news-info">
                                    <h3 class="text-capitalize">Ruangan bersih dan nyaman.</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!-- NEWS THUMB -->
                            <div class="news-thumb wow fadeInUp animated" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                                <a href="<?= base_url() ?>">
                                    <img src="<?= base_url('assets/images/bleach1.jpg') ?>" class="img-responsive" alt="">
                                </a>
                                <div class="news-info">
                                    <h3 class="text-capitalize">Peralatan canggih</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!-- NEWS THUMB -->
                            <div class="news-thumb wow fadeInUp animated" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                                <a href="<?= base_url() ?>">
                                    <img src="<?= base_url('assets/images/koral.jpg') ?>" class="img-responsive" alt="">
                                </a>
                                <div class="news-info">
                                    <h3 class="text-capitalize">Alat memenuhi standar</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-5">
                <div class="news-sidebar">
                    <div class="news-author">
                        <h4>Tentang Poli Gigi Alkindi</h4>
                        <p class="text-justify" style="text-indent: 45px;">Memilih perawatan gigi di Poli Gigi Alkindi, bisa dapat banyak keuntungan, lho! Mulai dari penanganan aman dan nyaman, hingga menikmati harga promo yang gila-gilaan. Selain itu, kamu juga bisa menikmati segala perawatan gigi dengan harga yang terjangkau.</p>
                    </div>

                    <div class="recent-post">
                        <h4>Layanan Kami Lainnya</h4>
                        <?php foreach ($post as $p) : ?>
                            <div class="media">
                                <div class="media">
                                    <div class="media-object pull-left">

                                    </div>
                                    <div class="media-body">
                                        <p class="media-heading"><a href="<?= base_url('') . $p->url . '/' . $p->slug ?>"><i class="fa fa-check"></i> <?= $p->title ?></a></p>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>


                </div>
            </div>

        </div>
    </div>
</section>