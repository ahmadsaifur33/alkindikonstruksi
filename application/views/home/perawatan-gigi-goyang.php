<section id="news-detail" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-7">
                <!-- NEWS THUMB -->
                <div class="news-detail-thumb">
                    <div class="news-image">
                        <img src="<?= base_url('assets/banner/') . $artikel->images; ?>" class="img-responsive" alt="">
                    </div>
                    <div class="row">

                        <div class="col-md-12 col-sm-12">
                            <!-- SECTION TITLE -->
                            <div class="section-title wow fadeInUp animated" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                <h2 class="text-capitalize">Spliting Gigi Goyang </h2>
                                <p class="text-justify">Spliting gigi adalah proses mengaitkan gigi goyang pada gigi yang masih sehat hingga jaringan di sekitarnya sembuh dan gigi kembali kokoh. Prosedur ini biasanya memakai kawat atau fiber</p>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <!-- SECTION TITLE -->
                            <div class="section-title wow fadeInUp animated" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                <h2 class="text-capitalize">Perawatan Gusi & Gigi Goyang </h2>
                                <p class="text-justify">Selain gigi berlubang, gigi goyang juga menjadi masalah umum yang sering ditemui di masyarakat. Gigi yang goyang bisa dirawat sehingga tidak harus dicabut pada kondisi tertentu. Untuk itu, sebaiknya periksakan gigi kamu yang goyang ke dokter gigi</p>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <!-- SECTION TITLE -->
                            <div class="section-title wow fadeInUp animated" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                <h2 class="text-capitalize">Penyebab Gigi Goyang </h2>
                                <p class="text-justify">Ada banyak faktor yang menyebabkan gigi goyang seperti :</p>
                                <ul>
                                    <li>
                                        <p class="text-left">Penyakit Gusi</p>
                                        <p>
                                            Penyakit gusi atau juga dikenal dengan nama periodontitis. Penyakit menimbulkan peradangan dan infeksi pada gusi yang bisa menyebabkan goyangnya pada gigi.</p>
                                    </li>
                                    <li>
                                        <p class="text-left">Kehamilan</p>
                                        <p class="text-left">Gigi goyang juga bisa dialami oleh ibu hamil. Karena peningkatan kadar estrogen dan progesteron selama kehamilan dapat memengaruhi tulang dan jaringan di mulut.</p>
                                    </li>
                                    <li>
                                        <p class="text-left">Cedera</p>
                                        <p class="text-left">Kecelakaan yang menyebabkan benturan juga dapat menyebabkan gigi goyang. Bahkan, hal ini juga dapat merusak gigi dan jaringan sekitarnya.</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <!-- SECTION TITLE -->
                            <div class="section-title wow fadeInUp animated" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                <h2 class="text-capitalize">Cara Mengatasi Gigi Goyang</h2>
                                <p class="text-capitalize">Untuk mempertahankan gigi yang sudah goyang agar tidak lepas di kemudian hari, umumnya dokter gigi akan melakukan tindakan medis yang memang perlu dilakukan, seperti: </p>
                                <ul>
                                    <li>
                                        <p class="text-left">Splinting</p>
                                        <img src="<?= base_url('assets/images/gigi1.png') ?>" class="img-responsive"> sumber : <a href="https://www.sehatq.com/artikel/splinting-gigi">https://www.sehatq.com/artikel/splinting-gigi </a>
                                        <p>
                                            Splinting gigi adalah prosedur untuk menguatkan kembali gigi yang goyang dengan mengikatnya pada gigi di sekitarnya yang masih kuat.
                                        </p>
                                    </li>
                                    <li>
                                        <p class="text-left">Kuretase</p>
                                        <img src="<?= base_url('assets/images/gigi2.png') ?>" class="img-responsive"> sumber : <a href="https://www.dictio.id/t/bagaimana-teknik-untuk-melakukan-kuretase/14694">https://www.dictio.id/t/bagaimana-teknik-untuk-melakukan-kuretase/14694</a>
                                        <p>Gigi goyang yang disebabkan oleh gusi yang bermasalah bisa diatasi dengan perawatan kuretase dengan menghilangkan jaringan infeksi pada gusi dan akar gigi.</p>
                                    </li>
                                    <li>
                                        <p class="text-left">Bedah Flap</p>
                                        <img src="<?= base_url('assets/images/gigi3.png') ?>" class="img-responsive"> sumber : <a href="https://www.dictio.id/t/apa-tujuan-dari-flap-gingiva/5673">https://www.dictio.id/t/apa-tujuan-dari-flap-gingiva/5673</a>
                                        <p>Operasi flap periodontal diperlukan untuk menghindari infeksi lebih lanjut pada gusi dan mulut. Terutama mengobati penyakit gusi yang parah.</p>
                                    </li>
                                    <li>
                                        <p class="text-left">Gum Lifting</p>
                                        <img src="<?= base_url('assets/images/gigi3.png') ?>" class="img-responsive"> sumber : <a href="https://www.signaturedentalwi.com/procedures/gum-contouring">https://www.signaturedentalwi.com/procedures/gum-contouring</a>
                                        <p>Gum-lifting merupakan perawatan yang dilakukan untuk memperbaiki estetika gigi dengan cara mengangkat dan membentuk gusi sehingga diperoleh bentuk gusi yang lebih simetris dan natural. </p>

                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <!-- SECTION TITLE -->
                            <div class="section-title wow fadeInUp animated" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                <h2 class="text-capitalize">Dokter Kami</h2>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <!-- SECTION TITLE -->
                            <div class="section-title wow fadeInUp animated" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                <a href="<?= base_url() ?>">
                                    <img src="<?= base_url('assets/images/fotodokter.jpg') ?>" class="img-responsive" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <!-- SECTION TITLE -->
                            <div class="section-title wow fadeInUp animated" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                <h2 class="text-capitalize">Dokter berpengalaman dan up-to-date</h2>
                                <p class="text-justify">Kami mengadakan pelatihan secara berkala kepada dokter gigi kami agar selalu up-to-date dan tanggap dalam memberikan pelayanan yang memuaskan.</p>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12">
                            <!-- SECTION TITLE -->
                            <div class="section-title wow fadeInUp animated" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                <h2 class="text-capitalize">Peralatan Lengkap</h2>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <!-- NEWS THUMB -->
                            <div class="news-thumb wow fadeInUp animated" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                                <a href="<?= base_url() ?>">
                                    <img src="<?= base_url('assets/images/room1.jpg') ?>" class="img-responsive" alt="">
                                </a>
                                <div class="news-info">
                                    <h3 class="text-capitalize">Ruangan bersih dan nyaman.</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!-- NEWS THUMB -->
                            <div class="news-thumb wow fadeInUp animated" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                                <a href="<?= base_url() ?>">
                                    <img src="<?= base_url('assets/images/bleach1.jpg') ?>" class="img-responsive" alt="">
                                </a>
                                <div class="news-info">
                                    <h3 class="text-capitalize">Peralatan canggih</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!-- NEWS THUMB -->
                            <div class="news-thumb wow fadeInUp animated" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                                <a href="<?= base_url() ?>">
                                    <img src="<?= base_url('assets/images/koral.jpg') ?>" class="img-responsive" alt="">
                                </a>
                                <div class="news-info">
                                    <h3 class="text-capitalize">Alat memenuhi standar</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-5">
                <div class="news-sidebar">
                    <div class="news-author">
                        <h4>Tentang Poli Gigi Alkindi</h4>
                        <p class="text-justify" style="text-indent: 45px;">Memilih perawatan gigi di Poli Gigi Alkindi, bisa dapat banyak keuntungan, lho! Mulai dari penanganan aman dan nyaman, hingga menikmati harga promo yang gila-gilaan. Selain itu, kamu juga bisa menikmati segala perawatan gigi dengan harga yang terjangkau.</p>
                    </div>

                    <div class="recent-post">
                        <h4>Layanan Kami Lainnya</h4>
                        <?php foreach ($post as $p) : ?>
                            <div class="media">
                                <div class="media">
                                    <div class="media-object pull-left">

                                    </div>
                                    <div class="media-body">
                                        <p class="media-heading"><a href="<?= base_url('') . $p->url . '/' . $p->slug ?>"><i class="fa fa-check"></i> <?= $p->title ?></a></p>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>


                </div>
            </div>

        </div>
    </div>
</section>