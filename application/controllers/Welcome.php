<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Dokter_model', 'dokter');
	}
	public function index()
	{
		$data['title'] = "Alkindi Royale ";
		$data['headmeta'] = " Alkindi  ";
		$data['description'] = "Comfortable Place";
		$data['keywords'] = "keywords";
		$data['subheadtitle'] = "Royale";
		$data['doctor'] = $this->dokter->fetch_data('dokter', ['id_subj' => "drg"])->result();
		$data['data'] =
			[
				'title' => 'Alkindi Royale',
				'headmeta' => 'Alkindi ',
				'subheadtitle' => 'Alkindi Poli Gigi',
			];
		$this->template->load('template', 'templating/home', $data);
	}

	public function about()
	{
		$data['title'] = "Alkindi Royale";
		$data['headmeta'] = " Alkindi  ";
		$data['data'] =
			[
				'title' => 'Comfortable Place',
				'headmeta' => 'Alkindi '
			];
		$this->template->load('template', 'home/about', $data);
	}
}
